# ELF Dependency Walker

It is a tool to analyst the dependence of ELF/Mach-O. When you port an ELF library to your own operating system, having a dependency graph is important.

## How to run

java -jar elf-dependency-walker.jar

After the main screen jumping out, click "Analyst" button and choose a ELF file. Then it will display the dependency tree as below.


![](https://www.quantr.foundation/wp-content/uploads/2022/05/elf-dependency-walker-2.png)

![](https://www.quantr.foundation/wp-content/uploads/2022/05/elf-dependency-walker-1.png)